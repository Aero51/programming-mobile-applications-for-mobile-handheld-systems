package com.example.lab_dailyselfie;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.provider.MediaStore;

public class MainActivity extends Activity {
	private SelfiesFragment mSelfiesFragment;
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		
		   if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
		        
		        String selfieName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());	        
		        SelfieModel newSelfie = new SelfieModel(selfieName);
		        newSelfie.setPic_bitmap(PictureHelper.getTempBitmap(this));
		        
		     
		        mSelfiesFragment.addSelfie(newSelfie);
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	}

	private static final int REQUEST_IMAGE_CAPTURE = 1;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			mSelfiesFragment= new SelfiesFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.mainLayout, mSelfiesFragment).commit();
			
		}
	}

	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.camera) {
			
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(PictureHelper.getTempFile(this))); 
		    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
		        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		    }
			
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 * 
	 * 
	 * 
	 * 
	 */
/*	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}*/
}
