package com.example.lab_dailyselfie;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class SelfiesGridAdapter extends CursorAdapter{

	private Context context;
	private String bitmapStoragePath;
	private SelfiesDatabase selfiesDatabase;
	private ArrayList<SelfieModel> selfie_model_list = new ArrayList<SelfieModel>();
	private static LayoutInflater mLayoutInflater = null;
	
	
	

	
	public SelfiesGridAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		this.context= context;
		selfiesDatabase = new SelfiesDatabase(context);
		selfiesDatabase.open();
		mLayoutInflater = LayoutInflater.from(context);
		bitmapStoragePath = PictureHelper.getBitmapStoragePath(context);
		
		//this.swapCursor(newCursor)
	}



	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		ViewHolder holder = (ViewHolder) view.getTag();
		int dimenPix = (int)context.getResources().getDimension(R.dimen.selfie_row_picture_width_height);
		holder.picture.setImageBitmap(PictureHelper.getScaledBitmap(cursor.getString(cursor.getColumnIndex(SelfiesDatabase.KEY_PICTURE_PATH)), dimenPix, dimenPix));
	}






	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = new ViewHolder();

		View newView = mLayoutInflater.inflate(R.layout.selfie_row, parent, false);
		//holder.name = (TextView)newView.findViewById(R.id.selfie_name_text_view);
		holder.picture = (ImageView)newView.findViewById(R.id.selfie_picture_image_view);

		newView.setTag(holder);

		return newView;
	}
	
	
static class ViewHolder {
		
		ImageView picture;
	}
	

}
