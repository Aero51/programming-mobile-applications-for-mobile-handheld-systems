package com.example.lab_dailyselfie;

import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class SelfieModel {
	
	
	private int id;
	private String name;
	private String path;
	
	private Drawable pic_drawable;
	private Bitmap pic_bitmap;
	

	
	public SelfieModel(String selfieName) {
		
		this.name = selfieName;

		
	}
	
	public Bitmap getPic_bitmap() {
		return pic_bitmap;
	}
	public void setPic_bitmap(Bitmap pic_bitmap) {
		this.pic_bitmap = pic_bitmap;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
