Coursera android part1&2  examples and labs
==================================================



##Examples:
###Week 1
#####Introduction to the Android Platform 
-HelloAndroid

#####Development Environment
 -TheAnswer


###Week 2
#####Application Fundamentals
-AppFundamentalsVideoSourceExamples (NOTE: This is just a set of different source code files, not a complete Android project)

#####The Activity Class
- MapLocation
- MapLocationFromContacts


###Week 3
#####The Intent Class
- HelloAndroidWithLogin

#####Permissions
- PermissionExampleBoom
- PermissionExampleBoomUser

#####The Fragment Class
- FragmentDynamicLayout
- FragmentDynamicLayoutWithActionBar
- FragmentProgrammaticLayout
- FragmentQuoteViewerWithActivity
- FragmentStaticConfigLayout
- FragmentStaticLayout



###Week 4
#####User Interface Classes
- google-play-services_lib NOTE: This is a library project used by some of the sample applications)
- HelloAndroidWithMenus
- UIAlertDialog
- UIAutoComplete
- UIButton
- UICheckBox
- UIDatePicker
- UIDatePickerFragment
- UIGallery
- UIGoogleMaps
- UIGridLayout
- UILinearLayout
- UIListView
- UIRadioGroup
- UIRatingsBar
- UIRelativeLayout
- UISampler
- UISpinner
- UITabLayout
- UITableLayout
- UITimePicker
- UITimePickerFragment
- UIToggleButton
- UIViewPager
- UIWebView



###Week 5
#####User Notifications
- NotificationStatusBar
- NotificationStatusBarWithCustomView
- NotificationToast
- NotificationToastWithCustomView

#####The BroadcastReceiver Class
- BcastRecCompBcast:
- BcastRecCompOrdBcast:
- BcastRecCompOrdBcastWithResRec:
- BcastRecSinBcastDynReg:
- BcastRecSinBcastStatReg:
- BcastRecStickyInt:

#####Threads, AsyncTasks and Handlers
- ThreadingAsyncTask
- ThreadingHandlerMessages
- ThreadingHandlerRunnable
- ThreadingNoThreading
- ThreadingRunOnUiThread
- ThreadingSimple
- ThreadingViewPost

#####Alarms
- AlarmCreateActivity

#####Networking
- NetworkingAndroidHttpClient
- NetworkingAndroidHttpClientJSON
- NetworkingAndroidHttpClientXML
- NetworkingSockets
- NetworkingURL


###Week 6
#####Graphics and Animation
- GraphicsBubbleProgram
- GraphicsBubbleXML
- GraphicsCanvasBubble
- GraphicsCanvasBubbleSurfaceView
- GraphicsFrameAnimation
- GraphicsPaint
- GraphicsShapeDraw
- GraphicsShapeDrawXML
- GraphicsTransitionDrawable
- GraphicsTweenAnimation
- GraphicsValueAnimator
- GraphicsViewPropertyAnimator

#####Multi-touch and Gestures
- TouchGestureViewFlipperTest
- TouchGestures
- TouchIndicateTouchLocation

#####MultiMedia
- AudioVideoAudioManager
- AudioVideoAudioRecording
- AudioVideoCamera
- AudioVideoRingtoneManager
- AudioVideoVideoPlay


###Week 7

#####Location and Maps
- LocationGetLocation
- LocationGetLocationServices
- MapEarthQuakeMap

#####Sensors
- SensorCompass
- SensorFilteredAccelerometer
- SensorShowValues


###Week 8


#####Data Management
- DataManagementFileExternalMemory
- DataManagementFileInternalMemory
- DataManagementPreferenceActivity
- DataManagementSQL
- DataManagementSharedPreference

#####The ContentProvider Class
- ContentProviderCustom
- ContentProviderCustomUser
- ContentProviderExample
- ContentProviderWithCursorLoader
- ContentProviderWithInsertionDeletion
- ContentProviderWithSimpleCursorAdapter

#####The Service Class
- LoggingServiceExample
- LoggingServiceWithMessenger
- LoggingServiceWithMessengerClient
- MusicPlayingServiceExample
- ServiceWithIPCExampleClient
- ServiceWithIPCExampleService





##Labs:
- ####DevelopmentEnvironment
- ####TheActivityClass
- ####Intents
- ####UserInterface

- ####AsyncTaskLab
- ####NotificationsLab
- ####GraphicsAndTouchLab
- ####androidpart2-Labs-LocationAndMaps-LocationLab



#Description of each example project:
##Week 1
####HelloAndroid     
- this is description 

####TheAnswer    
- this is description 


##Week 2
####AppFundamentalsVideoSourceExamples
- this is description 

####MapLocation
- this is description 

####MapLocationFromContacts
- this is description 



##Week 3
####HelloAndroidWithLogin
- this is description 

####PermissionExampleBoom
- this is description 

####PermissionExampleBoomUser
- this is description 

####FragmentDynamicLayout 
- this is description 

####FragmentDynamicLayoutWithActionBar 
- this is description 

####FragmentProgrammaticLayout 
- this is description 

####FragmentQuoteViewerWithActivity 
- this is description 

####FragmentStaticConfigLayout  
- this is description 

####FragmentStaticLayout 
- this is description 


##Week 4
####google-play-services_lib
- NOTE: This is a library project used by some of the sample 

####HelloAndroidWithMenus
- this is description 

####UIAlertDialog
- this is description 

####UIAutoComplete
- this is description 

####UIButton
- this is description 

####UICheckBox
- this is description 

####UIDatePicker
- this is description 

####UIDatePickerFragment
- this is description 

####UIGallery
- this is description 

####UIGoogleMaps
- this is description 

####UIGridLayout
- this is description 

####UILinearLayout
- this is description 

####UIListView
- this is description 

####UIRadioGroup
- this is description 

####UIRatingsBar
- this is description 

####UIRelativeLayout
- this is description 

####UISampler
- this is description 

####UISpinner
- this is description 

####UITabLayout
- this is description 

####UITableLayout
- this is description 

####UITimePicker
- this is description 

####UITimePickerFragment
- this is description 

####UIToggleButton
- this is description 

####UIViewPager
- this is description 

####UIWebView
- this is description 


##Week 5
####NotificationStatusBar
- this is description 

####NotificationStatusBarWithCustomView
- this is description 

####NotificationToast
- this is description 

####NotificationToastWithCustomView
- this is description 


####BcastRecCompBcast
- this is description 

####BcastRecCompOrdBcast
- this is description 

####BcastRecCompOrdBcastWithResRec
- this is description 

####BcastRecSinBcastDynReg
- this is description 

####BcastRecSinBcastStatReg
- this is description 

####BcastRecStickyInt
- this is description 


####ThreadingAsyncTask
- this is description 

####ThreadingHandlerMessages
- this is description 

####ThreadingHandlerRunnable
- this is description 

####ThreadingNoThreading
- this is description 

####ThreadingRunOnUiThread
- this is description 

####ThreadingSimple
- this is description 

####ThreadingViewPost
- this is description 


####AlarmCreateActivity
- this is description 


####NetworkingAndroidHttpClient
- this is description 

####NetworkingAndroidHttpClientJSON
- this is description 

####NetworkingAndroidHttpClientXML
- this is description 

####NetworkingSockets
- this is description 

####NetworkingURL
- this is description 



##Week 6
####GraphicsBubbleProgram
- this is description 

####GraphicsBubbleXML
- this is description 

####GraphicsCanvasBubble
- this is description 

####GraphicsCanvasBubbleSurfaceView
- this is description 

####GraphicsFrameAnimation
- this is description 

####GraphicsPaint
- this is description 

####GraphicsShapeDraw
- this is description 

####GraphicsShapeDrawXML
- this is description 

####GraphicsTransitionDrawable
- this is description 

####GraphicsTweenAnimation
- this is description 

####GraphicsValueAnimator
- this is description 

####GraphicsViewPropertyAnimator
- this is description 


####TouchGestureViewFlipperTest
- this is description 

####TouchGestures
- this is description 

####TouchIndicateTouchLocation
- this is description 



####AudioVideoAudioManager
- this is description 

####AudioVideoAudioRecording
- this is description 

####AudioVideoCamera
- this is description 

####AudioVideoRingtoneManager
- this is description 

####AudioVideoVideoPlay
- this is description 




##Week 7
####LocationGetLocation
- this is description 

####LocationGetLocationServices
- this is description

####MapEarthQuakeMap
- this is description


####SensorCompass
- this is description

####SensorFilteredAccelerometer
- this is description

####SensorShowValues
- this is description



##Week 8
####DataManagementFileExternalMemory
- this is description 

####DataManagementFileInternalMemory
- this is description

####DataManagementPreferenceActivity
- this is description

####DataManagementSQL
- this is description

####DataManagementSharedPreference
- this is description


####ContentProviderCustom
- this is description

####ContentProviderCustomUser
- this is description

####ContentProviderExample
- this is description

####ContentProviderWithCursorLoader
- this is description

####ContentProviderWithInsertionDeletion
- this is description

####ContentProviderWithSimpleCursorAdapter
- this is description



####LoggingServiceExample
- this is description

####LoggingServiceWithMessenger
- this is description

####LoggingServiceWithMessengerClient
- this is description

####MusicPlayingServiceExample
- this is description

####ServiceWithIPCExampleClient
- this is description

####ServiceWithIPCExampleService
- this is description





##References: 

[EventBus presentation](http://www.slideshare.net/greenrobot/eventbus-for-android-15314813)




##Todo:

~~stub~~

- add description for each project for easy search




##Screenshot:
![Screenshot](http://sportsnetwork.wpengine.netdna-cdn.com/wp-content/uploads/2010/10/the_social_network.png)


